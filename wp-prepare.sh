#!/bin/bash -e
WpPrepScriptVersion='0.0.12'
installDirForWp=$(pwd -P)

# Download: curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/wp-prepare.sh
# Run: sh wp-prepare.sh
echo '╔═════════════════════════════════════════════════════════════════════╗'
echo '║    WORDPRESS PREPARATION UTILITY  0.0.12                            ║'
echo '╠═════════════════════════════════════════════════════════════════════╣'
echo '║    This utility helps to download, prepare & install WordPress.     ║'
echo '║    More information: https://bitbucket.org/devaaja/wp-prepare/      ║'
echo '╚═════════════════════════════════════════════════════════════════════╝'
echo "█ Do you want install WordPress on $installDirForWp? (y/n)";
read -e runWpPrepareScript
if [ "$runWpPrepareScript" == 'n' ] ; then
	echo 'Proccess aborted.'
	exit
fi

# ========================================================================
# Check that current directory is clean and does not have unwanted content
if [[ "$installDirForWp" == "$HOME" ]]
then
	echo 'Proccess aborted.'
	echo 'Reason: Current directory is user home directory'
	echo 'Please select another directory'
	exit
fi
if [[ -d cms ]]
then
	echo 'Proccess aborted.'
	echo 'Reason: Current directory already contains subdirectory "cms".'
	echo 'To remove directory use command: rm -R cms'
	ls -l "$installDirForWp"
	exit
fi
if [[ -d wordpress ]]
then
	echo 'Proccess aborted.'
	echo 'Reason: Current directory already contains subdirectory "wordpress".'
	echo 'To remove directory use command: rm -R wordpress'
	ls -l "$installDirForWp"
	exit
fi
if [[ -f index.php ]]
then
	echo 'Proccess aborted.'
	echo 'Reason: Current directory already contains file "index.php".'
	echo 'To remove file use command: rm index.php'
	ls -l "$installDirForWp"
	exit
fi
if [[ -f index.html ]]
then
	echo 'Proccess aborted.'
	echo 'Reason: Current directory already contains file "index.html".'
	echo 'To remove file use command: rm index.html'
	ls -l "$installDirForWp"
	exit
fi
if [[ -f index.htm ]]
then
	echo 'Proccess aborted.'
	echo 'Reason: Current directory already contains file "index.htm".'
	echo 'To remove file use command: rm index.htm'
	ls -l "$installDirForWp"
	exit
fi

# ========================================================================
# Download and prepare WordPress
echo 'Downloading WordPress...'
curl https://wordpress.org/latest.zip --output wordpress.zip
echo 'Extracting WordPress...'
unzip -o wordpress.zip | awk 'BEGIN {ORS="·"} {if(NR%50==0)print "·"}'
echo ''
findVersonLine=$(grep '$wp_version\s*=' "$installDirForWp/wordpress/wp-includes/version.php")
findVersonNumber=$(echo "$findVersonLine" | awk 'match($0,/[0-9.]+/) {print substr($0,RSTART,RLENGTH)}')
echo "WordPress version: $findVersonNumber";
echo 'Renaming folder "wordpress" to "cms"'
mv wordpress cms
rm wordpress.zip

# ========================================================================
# Secure WordPress configuration file
echo 'Options -Indexes' >> cms/.htaccess
echo '<files wp-config.php>' >> cms/.htaccess
echo 'order allow,deny' >> cms/.htaccess
echo 'deny from all' >> cms/.htaccess
echo '</files>' >> cms/.htaccess
# Create index page
echo 'Creating index page (index.php)'
cp cms/index.php index.php
perl -i -pe's/wp-blog-header.php/cms\/wp-blog-header.php/g' index.php

# ========================================================================
# Create uploads directory
echo 'Creating uploads directory'
mkdir cms/wp-content/uploads
chmod 775 cms/wp-content/uploads
cp cms/wp-content/plugins/index.php cms/wp-content/uploads/index.php
echo 'Options -Indexes' >> cms/wp-content/uploads/.htaccess

# ========================================================================
# Lets do some cleanup
echo 'Removing plugin Hello Dolly'
rm cms/wp-content/plugins/hello.php
echo 'Removing plugin Akismet'
rm -R cms/wp-content/plugins/akismet
echo 'Removing theme Twenty Twenty'
rm -R cms/wp-content/themes/twentytwenty
echo 'Removing theme Twenty Nineteen'
rm -R cms/wp-content/themes/twentynineteen

# ========================================================================
#offer to install some plugins
echo '█ Do you want to add some plugins (y/n)'
read -e getAddPlugins
if [ "$getAddPlugins" == 'y' ] ; then

	# Lets add some plugins
	cd "$installDirForWp/cms/wp-content/plugins"

	# Yoast SEO
	echo '█ Do you want download plugin Yoast SEO? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading Yoast SEO...'
		curl https://downloads.wordpress.org/plugin/wordpress-seo.latest-stable.zip --output wordpres-seo.zip
		echo 'Extracting Yoast SEO...'
		unzip -o wordpres-seo.zip | awk 'BEGIN {ORS="·"} {if(NR%40==0)print "·"}'
		echo ''
		rm wordpres-seo.zip
	fi

	# Woocommerce
	echo '█ Do you want download plugin Woocommerce? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading Woocommerce...'
		curl https://downloads.wordpress.org/plugin/woocommerce.latest-stable.zip --output woocommerce.zip
		echo 'Extracting Woocommerce...'
		unzip -o woocommerce.zip | awk 'BEGIN {ORS="·"} {if(NR%80==0)print "·"}'
		echo ''
		rm woocommerce.zip
	fi

	# Paytrail for WooCommerce
	echo '█ Do you want download plugin Paytrail for WooCommerce? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading Paytrail for WooCommerce...'
		curl https://downloads.wordpress.org/plugin/paytrail-for-woocommerce.latest-stable.zip --output paytrail-for-woocommerce.zip
		echo 'Extracting Paytrail for WooCommerce...'
		unzip -o paytrail-for-woocommerce.zip | awk 'BEGIN {ORS="·"} {if(NR%20==0)print "·"}'
		echo ''
		rm paytrail-for-woocommerce.zip
	fi

	# Dummy Gateway for WooCommerce
	echo '█ Do you want download plugin Dummy Gateway for WooCommerce? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading Dummy Gateway for WooCommerce...'
		curl https://downloads.wordpress.org/plugin/dummy-gateway-for-woocommerce.latest-stable.zip --output dummy-gateway-for-woocommerce.zip
		echo 'Extracting Dummy Gateway for WooCommerce...'
		unzip -o dummy-gateway-for-woocommerce.zip | awk 'BEGIN {ORS="·"} {if(NR%1==0)print "·"}'
		echo ''
		rm dummy-gateway-for-woocommerce.zip
	fi

	# Disable Ccomments
	echo '█ Do you want download plugin Disable Ccomments? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading Disable Ccomments...'
		curl https://downloads.wordpress.org/plugin/disable-comments.latest-stable.zip --output disable-comments.zip
		echo 'Extracting Disable Ccomments...'
		unzip -o disable-comments.zip | awk 'BEGIN {ORS="·"} {if(NR%5==0)print "·"}'
		echo ''
		rm disable-comments.zip
	fi

	# Simple History
	echo '█ Do you want download plugin Simple History? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading Simple History...'
		curl https://downloads.wordpress.org/plugin/simple-history.latest-stable.zip --output simple-history.zip
		echo 'Extracting Simple History...'
		unzip -o simple-history.zip | awk 'BEGIN {ORS="·"} {if(NR%10==0)print "·"}'
		echo ''
		rm simple-history.zip
	fi

	# Elementor
	echo '█ Do you want download plugin Elementor? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading Elementor...'
		curl https://downloads.wordpress.org/plugin/elementor.latest-stable.zip --output elementor.zip
		echo 'Extracting Elementor...'
		unzip -o elementor.zip | awk 'BEGIN {ORS="·"} {if(NR%50==0)print "·"}'
		echo ''
		rm elementor.zip
	fi

	# One Click Unsplash Images
	echo '█ Do you want download plugin One Click Unsplash Images? (y/n)'
	read -e getPlugin
	if [ "$getPlugin" == 'y' ] ; then
		echo 'Downloading One Click Unsplash Images...'
		curl https://downloads.wordpress.org/plugin/instant-images.latest-stable.zip --output instant-images.zip
		echo 'Extracting One Click Unsplash Images...'
		unzip -o instant-images.zip | awk 'BEGIN {ORS="·"} {if(NR%5==0)print "·"}'
		echo ''
		rm instant-images.zip
	fi
#End of plugins installation
fi
# ========================================================================
# We need to add LevelUp Core plugin here too.

# ========================================================================
# Lets finish
cd "$installDirForWp"
echo 'WordPress successfully prepared for installation'
echo '█ Do you want to remove me - the WordPress Preparation Utility? (y/n)'
read -e removeMe
if [ "$removeMe" == 'y' ] ; then
	rm "$0"
fi
echo "Location: $installDirForWp"
