#!/bin/bash -e
#Script version
mampScriptVersion='0.0.12'
userSelecteDirr=$(pwd -P)
mampScriptBin="$0"

# Tell script version
if [[ $1 == '--version'  ]]
then
	echo $mampScriptVersion
	exit
fi

# Update script version
if [[ $1 == '--update'  ]]
then
	echo 'Updating WordPress Installation utility for MAMP Pro...'
	cd ~/Downloads
	curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/mampwpsite.sh
	chmod +x mampwpsite.sh
	sudo mv mampwpsite.sh /usr/local/bin/mampwpsite
	mampwpsite --version
	exit
fi

# Tell script location
if [[ $1 == '--location'  ]]
then
	echo 'You are running utility from:'
	echo "$mampScriptBin"
	exit
fi

clear
echo '╔═════════════════════════════════════════════════════════════════════╗'
echo '║    WORDPRESS INSTALLATION UTILITY FOR MAMP PRO                      ║'
echo '╚═════════════════════════════════════════════════════════════════════╝'
echo "█ Do you want to create new WordPress on site on MAMP? (y/n)";
read -e runScript
if [ "$runScript" == 'n' ] ; then
	echo 'Proccess aborted.'
	exit
fi
sitesDir="$HOME/Sites"
mampProApp='/Applications/MAMP PRO.app'
mampMySqlLock='/Applications/MAMP/tmp/mysql/mysql.sock.lock'
mampMySqlBin='/Applications/MAMP/Library/bin/mysql'
echo "Let's create new WordPress site on MAMP"
#Check that MAMP is installed
if [[ ! -d "$mampProApp"  ]]
then
	echo 'Proccess aborted! MAMP Pro is not installed!'
	exit
fi
if [[ ! -f "$mampMySqlBin"  ]]
then
	echo 'Proccess aborted! MySQL for MAMP is not found!'
	exit
fi
# Lets check that mySQL is running
while [ ! -f "$mampMySqlLock" ]; do
	echo "MySQL server is not running. Please start/restart MAMP."
	open -a "$mampProApp"
	echo '█ Do you want continue? (y/n)'
	read -e stopOrContinue
	if [ "$stopOrContinue" == 'n' ] ; then
		echo 'Proccess aborted! Please start/restart MAMP and try again.'
		exit
	fi
done
if ! command -v mysql &> /dev/null
then
	echo 'MySQL CLI is not properly configured and cannot be used by WP CLI.'
	echo "Creating symbolic link 'mysql' for $mampMySqlBin"
	sudo ln -s $mampMySqlBin /usr/local/bin/mysql
fi
# Lets get valid MySQl root password
while [ -z "$mysqlVersion" ]; do
	echo "█ Please provide root user password of MySQL and press enter."
	read -s mysqlRootPswd
	mysqlVersion=$(mysql -uroot -p$mysqlRootPswd --skip-column-names -e 'SELECT VERSION();' 2>/dev/null) && exit_status=$? || exit_status=$?
done
echo "MySQL Server Version: $mysqlVersion"
#Check that WP-CLI is available
if ! command -v wp &> /dev/null
then
	echo 'WP-CLI not installed. Installing WP-CLI...'
	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
	chmod +x wp-cli.phar
	sudo mv wp-cli.phar /usr/local/bin/wp
	wp --version
fi
# Check that sites directory exits
if [[ -d "$sitesDir" ]]
then
	echo "MAMP Pro Sites directory is located at $sitesDir"
else
	echo "Proccess aborted! MAMP Pro Sites directory is missing. Please create it first."
	echo "mkdir $sitesDir"
fi
cd "$sitesDir"
echo "Let's create root directory for new WordPress site."
echo "Only letters, numbers, undersores and dashes are allowed."
echo 'These names are already reserved:'
osascript <<EOD
tell application "$mampProApp"
	list all hosts
end tell
EOD
echo "Please type the name for new WordPress site:"
read wpFolderName
wpFolderName=$( echo $wpFolderName | perl -pe 's/[^a-zA-Z0-9\-\_]//g' )
wpDBName=$( echo $wpFolderName | perl -pe 's/[^a-zA-Z0-9\_]//g' )
wpDBName="wpd_$wpDBName"
wpDBPass=$(uuidgen | sed 's/[-]//g' | head -c 16; echo;)
wpHostDir="$sitesDir/$wpFolderName"
echo "Creating folder $wpHostDir"
mkdir "$wpHostDir"
# Check that directory is created
if [[ ! -d "$wpHostDir" ]]
then
	echo "Proccess aborted! Failed to create directory"
	echo "Please check permissions of dreictory $sitesDir"
	exit
fi
# Download and prepare WordPress
cd "$wpHostDir"
echo "Let's prepare WordPress for installation."
curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/wp-prepare.sh
sh wp-prepare.sh

# Check if WordPress script did run
cd $wpHostDir
if [[ ! -d cms ]]
then
	echo "Proccess aborted! WordPress folder 'cms' is missing."
	exit
fi
if [[ ! -f index.php ]]
then
	echo "Proccess aborted! WordPress index.php file is missing."
	exit
fi

#
echo 'WrodPress is ready for installation.'
#TODO: Add chech if database exists
#Lets create database
mysqlCreateDBs=$(mysql -uroot -p$mysqlRootPswd -e "CREATE DATABASE IF NOT EXISTS $wpDBName CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci" 2>/dev/null)
mysqlCreateUsr=$(mysql -uroot -p$mysqlRootPswd -e "CREATE USER '$wpDBName'@'%' IDENTIFIED BY '$wpDBPass';" 2>/dev/null)
mysqlAddPrivil=$(mysql -uroot -p$mysqlRootPswd -e "GRANT ALL PRIVILEGES ON $wpDBName.* TO '$wpDBName'@'%';" 2>/dev/null)
mysqlFlushPriv=$(mysql -uroot -p$mysqlRootPswd -e "FLUSH PRIVILEGES;" 2>/dev/null)
mysqlPortNumbe=$(mysql -uroot -p$mysqlRootPswd -e "SELECT @@GLOBAL.port AS 'port';" --skip-column-names 2>/dev/null)

#Create WordPress Configuration
echo 'Let`s create new host on MAMP'
echo 'Please save new host on MAMP and return here to complete the installation.'
# Use AppleScript to add new host on mamp
osascript <<EOD
tell application "$mampProApp"
	stop all servers
	add host "$wpFolderName" document root "$wpHostDir" with ssl
	start all servers
end tell
EOD

# Lets wait until user is ready
while [ "$isHostReady" != 'y' ]; do
	echo "█ Did you save new host on MAMP Pro? (y/n)"
	read isHostReady
done
#read newHostPortNumber
wpHomeUrl="https://$wpFolderName:8890"
wpSiteUrl="$wpHomeUrl/cms"
#Lets install WordPress finally
echo 'Installing WordPress...'
newAdminName='devaaja'
#Generate random password for WP administrator.
nreAdminPswd="P$(uuidgen | sed 's/[-]//g' | head -c 15)"
#Create WordPress configuration
wp config create --dbname="$wpDBName" --dbuser="$wpDBName" --dbpass="$wpDBPass" --dbhost="127.0.0.1:$mysqlPortNumbe" --dbcollate='utf8mb4_unicode_ci' --dbcharset='utf8mb4' --locale='en_US'
#Complete installation of WordPress
wp core install --url="$wpSiteUrl" --title='Wordpress Site' --admin_user="$newAdminName" --admin_password="$nreAdminPswd" --admin_email='devaaja@levelup.fi' --skip-email
wp option update home $wpHomeUrl
wp option update permalink_structure '/%category%/%postname%/'
wp comment delete $(wp comment list --format=ids)
findVersonLine=$(grep '$wp_version\s*=' "$wpHostDir/cms/wp-includes/version.php")
findVersonNumber=$(echo "$findVersonLine" | awk 'match($0,/[0-9.]+/) {print substr($0,RSTART,RLENGTH)}')
echo 'WordPress installation complete.'
echo "WordPress version: $findVersonNumber";
echo "Host name: $wpFolderName"
echo "Root directory: $wpHostDir"
echo "User: $newAdminName"
echo "Pswd: $nreAdminPswd"
echo 'Please change password immidiately'.
echo "Site URL: $wpHomeUrl"
open "$wpHostDir"
open "$wpHomeUrl/cms/wp-admin"
