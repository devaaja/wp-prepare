#!/bin/bash -e
#Script version
wpBuildcriptVersion='0.0.12'
userSelectedDir=$(pwd -P)
wpBuildScriptBin="$0"
wpBuildScriptConf="$HOME/etc/wpbuild.conf"
wpBuildHostsJson="$OME/tmp/domains-list.json"
wpBuildUser=$USER

if [[ $1 == '--version'  ]]
then
	echo $wpBuildcriptVersion
	exit
fi

# Update script version
if [[ $1 == '--update'  ]]
then
	echo 'Updating WordPress preparation utility...'
	cd "$HOME/tmp"
	curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/wpbuild.sh
	chmod +x wpbuild.sh
	mv wpbuild.sh "$wpBuildScriptBin"
	cd "$HOME"
	wpbuild --version
	exit
fi

# Tell script location
if [[ $1 == '--location'  ]]
then
	echo 'Location of wpuild:'
	echo $wpBuildScriptBin
	exit
fi

# Tell script to read configuration
if [[ $1 == '--readconf'  ]]
then
	if [[ ! -f "$wpBuildScriptConf"  ]]
	then
		echo 'Configuration file not created.'
		exit
	fi
	echo 'Configuration:'
	cat $wpBuildScriptConf
	exit
fi

# Tell script to remove configuraiton
if [[ $1 == '--removeconf'  ]]
then
	rm $wpBuildScriptConf
	exit
fi

# Tell script location
if [[ $1 == '--makeconf'  ]]
then
	if [[ -f "$wpBuildScriptConf"  ]]
	then
		echo 'Configuration file already exists.'
		exit
	fi
	echo 'Creating configuration file'
	touch $wpBuildScriptConf
	if [[ ! -f "$wpBuildScriptConf"  ]]
	then
		echo 'Failed to create configuraiton file.'
		exit
	fi
	echo 'wpBuildSubDomDir="_subdomains"' | tee -a  $wpBuildScriptConf > /dev/null
	echo 'wpBuildSubDomPath="$HOME/$wpBuildSubDomDir"' | tee -a  $wpBuildScriptConf > /dev/null
	echo 'Configuraton file created. You can now edit it:'
	echo 'wpbuild --editconf'
	exit
fi

# Tell script location
if [[ $1 == '--editconf'  ]]
then
	if [[ ! -f "$wpBuildScriptConf"  ]]
	then
		echo 'Configuration file is missing. Use command below to create configuration file:'
		echo 'wpbuild --makeconf'
		exit
	fi
	nano "$wpBuildScriptConf"
fi

#Show banner
echo '╔═════════════════════════════════════════════════════════════════════╗'
echo '║    WORDPRESS INSTALLATION UTILITY FOR CPANEL  0.0.11                ║'
echo '╚═════════════════════════════════════════════════════════════════════╝'

if [[ ! -f "$wpBuildScriptConf"  ]]
then
	echo 'Proccess aborted! Configuration file not found!'
	echo 'Use command below to create configuration file:'
	echo 'wpbuild --makeconf'
	exit
fi

#Load configuration
. $wpBuildScriptConf

#As name for subdomain
echo 'Root directry for subdomains:'
echo "$wpBuildSubDomPath"

echo "Please type the name for new WordPress site:"
read wpBuildFolderName
wpBuildFolderName=$( echo $wpBuildFolderName | perl -pe 's/[^a-zA-Z0-9]//g' )
wpBuildFolderPath="$wpBuildSubDomPath/$wpBuildFolderName"

echo "Chosen name: $wpBuildFolderName"

#Get domains list
uapi --output=json DomainInfo list_domains > $wpBuildHostsJson
#Get main domain
wpBuildMainDomain=$(jq -r '.result.data.main_domain' $wpBuildHostsJson)
#Create subdomain name
wpBuildSubdomain="$wpBuildFolderName.$wpBuildMainDomain"
wpBuildWpHome="https://$wpBuildSubdomain"
wpBuildWpSite="https://$wpBuildSubdomain/cms"
#Tell to user gathered data
echo -e "Main Domain:\t$wpBuildMainDomain"
echo -e "New Subbomain:\t$wpBuildSubdomain"

#Check that subdomain not created yet
wpBuildSubdomainExits=$(jq ".result.data.sub_domains|any(. == \"$wpBuildSubdomain\")" $wpBuildHostsJson)
if $wpBuildSubdomainExits;
then
	echo "Proccess aborted! Subdomain '$wpBuildSubdomain' alredy exists."
	echo "These names are already in use:"
	jq -cr '.result.data.sub_domains[]' $wpBuildHostsJson | while read i; do
		echo -e "\t$i"
	done
	exit
fi

# Check if directory already
if [[ -d "$wpBuildFolderPath" ]]
then
	echo "Proccess aborted! Folder '$wpBuildFolderName' already exits."
	echo "Please try another name."
	exit
fi

#Create directory for subdomain
cd $wpBuildSubDomPath
mkdir $wpBuildFolderPath
cd $wpBuildFolderPath

#Get Wordpres preparation script and run it
echo "Let's download and prepare WordPress."
curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/wp-prepare.sh
sh wp-prepare.sh

# Check if WordPress script did run
cd $wpBuildFolderPath
if [[ ! -d cms ]]
then
	echo "Proccess aborted! WordPress folder 'cms' is missing."
	exit
fi
if [[ ! -f index.php ]]
then
	echo "Proccess aborted! WordPress index.php file is missing."
	exit
fi

echo 'WrodPress is ready for installation.'

#Lets create database
echo "Creating WordPress database..."
wpBuildDbName="${USER}_${wpBuildFolderName}"
wpBuildDBPass="P$(cat /proc/sys/kernel/random/uuid | sed 's/[-]//g' | head -c 15)"
echo -e "DB Name:\t$wpBuildDbName"
echo -e "DB User:\t$wpBuildDbName"
echo -e "DB Pswd:\t$wpBuildDBPass"
wpBuildCreateDBd=$(uapi Mysql create_database name="$wpBuildDbName" >/dev/null)
wpBuildCreateDBo=$(uapi Mysql create_user name="$wpBuildDbName" password="$wpBuildDBPass" >/dev/null)
wpBuildCreateDBp=$(uapi Mysql set_privileges_on_database user="$wpBuildDbName" database="$wpBuildDbName" privileges=ALL >/dev/null)
wpBuildCreateDBf=$(mysql -u$wpBuildDbName -p$wpBuildDBPass -e "ALTER DATABASE $wpBuildDbName CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci" 2>/dev/null)

#Lets install WordPress finally
echo 'Installing WordPress...'
wpBuildAdminUser='devaaja'
wpBuildAdminPswd="P$(cat /proc/sys/kernel/random/uuid | sed 's/[-]//g' | head -c 15)"
#Create WordPress configuration
wp config create --dbname="$wpBuildDbName" --dbuser="$wpBuildDbName" --dbpass="$wpBuildDBPass" --dbhost="localhost" --dbcollate='utf8mb4_unicode_ci' --dbcharset='utf8mb4' --locale='fi_FI'
#Complete installation of WordPress
wp core install --url="$wpBuildWpSite" --title='WP Site' --admin_user="$wpBuildAdminUser" --admin_password="$wpBuildAdminPswd" --admin_email='devaaja@levelup.fi' --skip-email
wp option update home $wpBuildWpHome
wp option update permalink_structure '/%category%/%postname%/'
wp comment delete $(wp comment list --format=ids)
findWpVersonLine=$(grep '$wp_version\s*=' "$wpBuildFolderPath/cms/wp-includes/version.php")
findWpVersonNumber=$(echo "$findWpVersonLine" | awk 'match($0,/[0-9.]+/) {print substr($0,RSTART,RLENGTH)}')

#Output result
echo 'WordPress installation complete!'
echo -e "Wordpress:\t$findWpVersonNumber";
echo -e "Host:\t$wpBuildSubdomain"
echo -e "Path:\t$wpBuildFolderPath"
echo -e "URL:\t$wpBuildWpHome"
echo -e "User:\t$wpBuildAdminUser"
echo -e "Pswd:\t$wpBuildAdminPswd"
echo 'Please change password immidiately'.

#Add subdomain https://api.docs.cpanel.net/openapi/cpanel/operation/addsubdomain/
echo "Creating subdomain $wpBuildSubdomain"
uapi --output=json SubDomain addsubdomain domain="$wpBuildFolderName" rootdomain="$wpBuildMainDomain" dir="/$wpBuildSubDomDir/$wpBuildFolderName" > "$HOME/tmp/wpbuild-subdomain.json"
wpBuildSubdomainCreationStatus=$(jq -r '.result.status' "$HOME/tmp/wpbuild-subdomain.json")

if [[ $wpBuildSubdomainCreationStatus=1 ]]
then
	echo "Subdomain successfully created!"
else
	echo "Failed to create subdomain!"
	echo "Please go to https://cpanel.$wpBuildMainDomain/ and add subomain manually:"
	echo "Name: $wpBuildFolderName"
	echo "Folder: $wpBuildSubDomDir/$wpBuildFolderName"
fi

echo "Adding SSL to subdomain."
uapi SSL remove_autossl_excluded_domains domains="$wpBuildSubdomain"
uapi SSL start_autossl_check

if [[ $wpBuildMainDomain='develup.fi' ]]
then
	#Note about DNS record
	echo "Please go to https://oma.zoner.fi and add DNS records for '$wpBuildSubdomain'"
	echo -e "Type:\tCNAME"
	echo -e "Name:\t$wpBuildSubdomain"
	echo -e "TTL:\t300"
	echo -e "Sisaltö:\t$wpBuildMainDomain"
fi
