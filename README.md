# WordPress Preparation Script

Toolset which helps to download, prepare and install WordPress.

WordPress Preparation Script:

1. Downloads latest version of WordPress
2. Creates proper structure for new site
3. Offers to download some plugins

## Usage
Change in terminal the current working directory to your site root folder and run code below:

```sh
curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/wp-prepare.sh
sh wp-prepare.sh
```

Follow on-screen instuctions.

## WPBuilder for CPanel

WPBuilder for CPanel helps to easily create new WordPress subdomains on CPanel servers for development purposes.

1. Prepares directory for new subdomain.
2. Creates new database for new subdomain.
3. Downloads and runs `wp-prepare.sh` script.
4. Installs WordPress.
5. Does some cleanups on WordPress.
6. Creates new subdomain.

### Installation

```sh
curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/wpbuild.sh
chmod +x wpbuild.sh
sudo mv wpbuild.sh "$HOME/bin/wpbuild"
wpbuild --version
```

### Usage

```sh
wpbuild
```

Follow on-screen instuctions.

### Update

You can update WPBuilder Installer for CPanel by using `--update` argument.

```sh
wpbuild --update
```

## WordPress Installer for MAMP Pro

WordPress Installer for MAMP helps to easily create new WordPress sites on MAMP for development purposes.

1. Prepares directory for new MAMP host.
2. Creates new database for new MAMP host.
3. Creates new MAMP host.
4. Downloads and runs `wp-prepare.sh` script.
5. Installs WordPress.
6. Does some cleanups on WordPress.

### Installation

```sh
curl -O https://bitbucket.org/devaaja/wp-prepare/raw/master/mampwpsite.sh
chmod +x mampwpsite.sh
sudo mv mampwpsite.sh /usr/local/bin/mampwpsite
mampwpsite --version
```

### Usage

```sh
mampwpsite
```

Follow on-screen instuctions.

*PS! There is still bug on MAMP Pro, which does not allow save new hosts automatically. **You have to manually save automatically created host.*** Script will prompt to you for that.

### Update

You can update WordPress Installer for MAMP by using `--update` argument.

```sh
mampwpsite --update
```

## Release Notes

### 0.0.10

* Added WPBuild script which enables easily to create WordPress subdomains on CPanel servers.
* Documentation update

### 0.0.8

* WordPress Installer for MAMP
  * Random WP administrator password generated now.
  * User interaction messages improved.
* WordPress Preparation Utility
  * Shows version number of downloaded WordPress

### 0.0.7

* Various improvements.

### 0.0.6

* WordPress Installer for MAMP
  * Added self update functionality: `mampwpsite --update`.


### 0.0.3

* Plugin "One Click Unsplash Images" added into plugins pickup list.
* MAMP Pro WP Installer opens root directory on Finder.

### 0.0.2

* MAMP Pro related functionality imporoved. Hosts are created automatically on MAMP Pro now.

### 0.0.1

* Initial release
